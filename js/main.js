var typed = new Typed(".typed", {
  // Waits 1000ms after typing "First"
  strings: [
    "We're doing something new.",
    "PHP...",
    "Javascript...",
    "HTML...",
    "CSS...",
    "Icons...",
    "We provide all the development platform <i>YOU</i> need for YOU!",
    "Creating and managing web-based projects will be easier than you'd expect.",
    "Blue Project comes with new version!..."
    ],
  smartBackspace: true, // Default value,
  typeSpeed: 20
});